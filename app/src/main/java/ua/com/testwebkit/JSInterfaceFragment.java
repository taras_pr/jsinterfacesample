package ua.com.testwebkit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

public class JSInterfaceFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_web, container, false);
        context = container.getContext();


        webView = v.findViewById(R.id.webView);
        cardView = v.findViewById(R.id.cardView);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl("file:///android_res/raw/index.html");
                cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_res/raw/index.html");

        jsInterface = new JSInterface(context);
        webView.addJavascriptInterface(jsInterface, "JSInterface");

        return v;
    }


}
