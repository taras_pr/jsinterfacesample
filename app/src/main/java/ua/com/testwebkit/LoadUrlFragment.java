package ua.com.testwebkit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoadUrlFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_web, container, false);

        context = container.getContext();
        webView = v.findViewById(R.id.webView);
        cardView = v.findViewById(R.id.cardView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(
                "javascript:(" +
                        "function() { " +
                        "var element = document.getElementById('webButton');"
                        + "element.style.background='#aaffaa';" +
                        "})()");



        jsInterface = new JSInterface(context);
        webView.addJavascriptInterface(jsInterface, "JSInterface");

        webView.loadUrl("file:///android_res/raw/index.html");

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.setWebViewClient(new WebViewClient(){
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        webView.loadUrl(
                                "javascript:(" +
                                        "function() { " +
                                        "var element = document.getElementById('webButton');"
                                        + "element.style.background='#ffaaaa';" +
                                        "})()");
                    }

                });
                webView.loadUrl("file:///android_res/raw/index.html");

                cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));

            }
        });

        return v;
    }
}
