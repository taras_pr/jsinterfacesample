package ua.com.testwebkit;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

public class BaseFragment extends Fragment {

    Context context;
    WebView webView;
    CardView cardView;

    JSInterface jsInterface;

    protected class JSInterface {
        Context mContext;

        JSInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void toastMe(String text) {
            cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }


    }

}
